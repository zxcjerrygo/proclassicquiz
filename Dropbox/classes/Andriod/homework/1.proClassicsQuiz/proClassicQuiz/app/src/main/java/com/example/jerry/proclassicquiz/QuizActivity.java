package com.example.jerry.proclassicquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class QuizActivity extends AppCompatActivity {

    private EditText editName;
    private Button latinButton, greekButton, mixedButton;
    private Button exitButton;
    private String name;

    private void startMe(int mode) {
        name = editName.getText().toString();
        QuizTracker.getInstance().setName(name);
        askQuestion(mode);
    }

    private void askQuestion(int mode) {
        QuizTracker.getInstance().setQuestionNum(1);
        Intent intent = new Intent(this, QuestionActivity.class);
        intent.putExtra("mode", mode);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        editName = findViewById(R.id.editName);
        latinButton = findViewById(R.id.latinButton);
        greekButton = findViewById(R.id.greekButton);
        mixedButton = findViewById(R.id.mixedButton);
        exitButton = findViewById(R.id.exitButton);

        latinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMe(1);
            }
        });
        greekButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMe(2);
            }
        });
        mixedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMe(3);
            }
        });
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    //these are for the menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuExit:
                finish();
                return true;
            case R.id.latinStart:
                startMe(1);
                return true;
            case R.id.greekStart:
                startMe(2);
                return true;
            case R.id.mixedStart:
                startMe(3);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
