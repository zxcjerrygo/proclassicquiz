package com.example.jerry.proclassicquiz;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class QuestionActivity extends AppCompatActivity {

    private static final String QUESTION = "om.example.jerry.proclassicquiz.QUESTION";

    private static final String DELIMITER = "\\|";
    private static final int NUM_ANSWERS = 5;
    private static final int ENGLISH = 0;
    private static final int LATIN = 1;
    private static final int GREEK = 2;
    private static final int MIXED = 3;

    private Random mRandom;

    private Question mQuestion;
    private String[] mClassicWords;
    private boolean mItemSelected = false;
    //make these members
    private TextView mQuestionNumberTextView;
    private RadioGroup mQuestionRadioGroup;
    private TextView mQuestionTextView;
    private Button mSubmitButton;
    private Button mQuitButton;
    //make mode to store the button of the begining
    protected int mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        Intent intent = getIntent();
        mode = intent.getIntExtra("mode", 0);

        //generate a question
        mClassicWords = getResources().getStringArray(R.array.classic_words);

        //get refs to inflated members
        mQuestionNumberTextView = findViewById(R.id.questionNumber);
        mQuestionTextView = findViewById(R.id.questionText);
        mSubmitButton = findViewById(R.id.submitButton);

        //init the random
        mRandom = new Random();

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                submit();
            }
        });


        //set quit button action
        mQuitButton = findViewById(R.id.quitButton);
        mQuitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayResult();
            }
        });

        mQuestionRadioGroup = findViewById(R.id.radioAnswers);
        //disallow submitting until an answer is selected
        mQuestionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                mSubmitButton.setEnabled(true);
                mItemSelected = true;
            }
        });
        // fireQuestion();
        fireQuestion(savedInstanceState, mode);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //pass the question into the bundle when I have a config change
        outState.putSerializable(QuestionActivity.QUESTION, mQuestion);
    }

    private void fireQuestion(int mode){
        mQuestion = getQuestion(mode);
        populateUserInterface(mode);
    }

    //overloaded to take savedInstanceState
    private void fireQuestion(Bundle savedInstanceState, int mode){
        if (savedInstanceState == null ){
            mQuestion = getQuestion(mode);
        } else {
            mQuestion = (Question) savedInstanceState.getSerializable(QuestionActivity.QUESTION);
        }
        populateUserInterface(mode);
    }

    private void submit() {
        Button checkedButton = findViewById(mQuestionRadioGroup.getCheckedRadioButtonId());
        String guess = checkedButton.getText().toString();
        //see if they guessed right
        if (mQuestion.getAnswer().equals(guess)) {
            QuizTracker.getInstance().answeredRight();
        } else {
            QuizTracker.getInstance().answeredWrong();
        }
        if (QuizTracker.getInstance().getTotalAnswers() < Integer.MAX_VALUE) {
            //increment the question number
            QuizTracker.getInstance().incrementQuestionNumber();
            fireQuestion(mode);
        } else {
            displayResult();
        }

    }

    private void populateUserInterface(int mode) {
        //take care of button first
        mSubmitButton.setEnabled(false);
        mItemSelected = false;

        //populate the QuestionNumber textview
        String questionNumberText = getResources().getString(R.string.questionNumberText);
        int number = QuizTracker.getInstance().getQuestionNum();
        mQuestionNumberTextView.setText(String.format(questionNumberText, number));

        //set question text
        mQuestionTextView.setText(mQuestion.getQuestionText());

        //will generate a number 0-4 inclusive
        int randomPosition = mRandom.nextInt(NUM_ANSWERS);
        int counter = 0;
        mQuestionRadioGroup.removeAllViews();
        //for each of the 5 wrong answers
        for (String wrongAnswer : mQuestion.getWrongAnswers()) {
            if (counter == randomPosition) {
                //insert the cor answer
                addRadioButton(mQuestionRadioGroup, mQuestion.getAnswer());
            } else {
                addRadioButton(mQuestionRadioGroup, wrongAnswer);
            }
            counter++;
        }
    }

    private void addRadioButton(RadioGroup questionGroup, String text) {
        RadioButton button = new RadioButton(this);
        button.setText(text);
        button.setTextColor(Color.WHITE);
        button.setButtonDrawable(android.R.drawable.btn_radio);
        questionGroup.addView(button);
    }


    private Question getQuestion(int mode) {
        //generate corr answer
        String[] strAnswers = getRandomClassicWord();
        mQuestion = new Question(strAnswers[ENGLISH], strAnswers[LATIN], strAnswers[GREEK]);
        // make sure we have an answer first
        mQuestion.makeAnswer(mode);

        //generates 5 wrong answers
        while (mQuestion.getWrongAnswers().size() < NUM_ANSWERS) {
            String[] strClassWord = getRandomClassicWord();
            //if the one we picked is equal to the answer OR
            //if we already picked this one
            while (strClassWord[ENGLISH].equals(strAnswers[ENGLISH]) ||
                    mQuestion.getWrongAnswers().contains(strClassWord[LATIN]) ||
                    mQuestion.getWrongAnswers().contains(strClassWord[GREEK])) {
                //then we need pick another one
                strClassWord = getRandomClassicWord();
            }

            if (mode != MIXED)
                mQuestion.addWrongAnswer(strClassWord[mode]);
            else {
                // randomly choose Latin or Greek to add to the wrong answer pool
                int index = mRandom.nextInt(2);
                mQuestion.addWrongAnswer(strClassWord[index+1]);
            }
        }
        return mQuestion;
    }

    private String[] getRandomClassicWord() {
        int index = mRandom.nextInt(mClassicWords.length);
        return mClassicWords[index].split(DELIMITER);
    }

    private void displayResult(){
        Intent intent = new Intent(this, ResultsActivity.class);
        intent.putExtra("mode", mode);
        startActivity(intent);
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_question, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuQuit:
                displayResult();
                return true;
            case R.id.menuSubmit:
                if(mItemSelected){
                    submit();
                }
                else{
                    Toast toast = Toast.makeText(this, getResources().getText(R.string.pleaseSelectAnswer), Toast.LENGTH_SHORT);
                    toast.show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
