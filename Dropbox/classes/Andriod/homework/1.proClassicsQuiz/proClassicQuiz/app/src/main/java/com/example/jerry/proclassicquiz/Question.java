package com.example.jerry.proclassicquiz;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Question implements Serializable {

    private static final long serialVersionUID = 6546546516546843135L;

    private String mEnglish;
    private String mLatin;
    private String mGreek;
    private String mAnswer;
    private Random mRandom;
    private Set<String> mWrongAnswers = new HashSet<String>();

    public Question(String english, String latin, String greek) {
        this.mEnglish = english;
        this.mLatin = latin;
        this.mGreek = greek;
        mRandom = new Random();
    }

    private String getLatin() {
        return mLatin;
    }

    private String getGreek() {
        return mGreek;
    }

    public String getAnswer() {
        return mAnswer;
    }

    public void makeAnswer(int mode) {
        if(mode == 1)
            mAnswer = getLatin();
        else if(mode == 2)
            mAnswer = getGreek();
        else {
            int index = mRandom.nextInt(2);
            mAnswer = (index == 0 ? getLatin() : getGreek());
        }
    }

    public Set<String> getWrongAnswers() {
        return mWrongAnswers;
    }

    public void addWrongAnswer(String wrongAnswer){
        mWrongAnswers.add(wrongAnswer);
    }

    public String getQuestionText(){
        return "What is " + mEnglish + "?";
    }

}
